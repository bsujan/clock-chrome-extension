## To load your extension in Chrome
- Create a folder in your system and clone this project to that folder
- Open up chrome://extensions/ in your browser and click “Developer mode” in the top right. Now click “Load unpacked extension…” and select the extension’s directory. You should now see your extension in the list.

Preview: [http://sujanbyanjankar.com.np/clock](http://sujanbyanjankar.com.np/clock)