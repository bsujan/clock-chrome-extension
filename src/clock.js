var angles = [];
angles['hours'] = 0;
angles['minutes'] = 0;
angles['seconds'] = 0;

var x = document.getElementById("myAudio");
x.volume = 0.05;

function initLocalClocks() {
  // Get the local time using JS
  var date = new Date;
  var seconds = date.getSeconds();
  var minutes = date.getMinutes();
  var hours = date.getHours();

  var year = date.getFullYear();
  var month = date.getMonth();
  var day = date.getDate();
  var weekday = getWeekDay(date);

  var today = getMonthName(month) + ' ' + day + ', ' + year;

  console.log(today)

  //document.getElementById('year').innerHTML = year;
  //document.getElementById('month').setAttribute('data-month', month);
  //document.getElementById('day').setAttribute('data-day', day);

  document.getElementById('fulldate').innerHTML = today;
  document.getElementById('weekday').innerHTML = weekday;

  //console.log(year, month, day);

  // Create an object with each hand and it's angle in degrees
  var hands = [
    {
      hand: 'hours',
      angle: (hours * 30) + (minutes / 2)
    },
    {
      hand: 'minutes',
      angle: (minutes * 6)
    },
    {
      hand: 'seconds',
      angle: (seconds * 6)
    }
  ];
  // Loop through each of these hands to set their angle
  for (var j = 0; j < hands.length; j++) {
    angles[hands[j].hand] = hands[j].angle;
    var elements = document.querySelectorAll('.' + hands[j].hand);
    for (var k = 0; k < elements.length; k++) {
      elements[k].style.webkitTransform = 'rotateZ(' + hands[j].angle + 'deg)';
      elements[k].style.transform = 'rotateZ(' + hands[j].angle + 'deg)';
      // If this is a minute hand, note the seconds position (to calculate minute position later)
      if (hands[j].hand === 'minutes') {
        elements[k].parentNode.setAttribute('data-second-angle', hands[j + 1].angle);
      }
    }
  }

  setInterval(function () { tick(); }, 1000);
}

function tick(hand = 'seconds') {
  var el = document.getElementById(hand);

  var angle = 0
  if (hand == 'seconds') {
    angle = 360 / 60
    angle = angle + angles['seconds']
    angles['seconds'] = angle;
    if (angle % 360 == 0)
      tick('minutes')
  }


  if (hand == 'minutes') {
    angle = 360 / 60
    angle = angle + angles['minutes']
    angles['minutes'] = angle;
    tick('hours')
  }


  if (hand == 'hours') {
    angle = (360 / 60) / 12
    angle = angle + angles['hours']
    angles['hours'] = angle;
  }

  document.getElementById(hand).style.webkitTransform = 'rotateZ(' + angle + 'deg)';
  document.getElementById(hand).style.transform = 'rotateZ(' + angle + 'deg)';


  setTimeout(function () {
    x.play();
  }, 600)
}

function getCurrentAngle(el) {
  var st = window.getComputedStyle(el, null);
  var tm = st.getPropertyValue("-webkit-transform") ||
    st.getPropertyValue("-moz-transform") ||
    st.getPropertyValue("-ms-transform") ||
    st.getPropertyValue("-o-transform") ||
    st.getPropertyValue("transform") ||
    "none";
  if (tm != "none") {
    var values = tm.split('(')[1].split(')')[0].split(',');
    var angle = Math.round(Math.atan2(values[1], values[0]) * (180 / Math.PI));
    return (angle < 0 ? angle + 360 : angle); //adding 360 degrees here when angle < 0 is equivalent to adding (2 * Math.PI) radians before
  }
  return 0;
}


function getWeekDay(date) {
  var weekdays = new Array(
    "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
  );
  var day = date.getDay();
  return weekdays[day];
}

function getMonthName(monthIdx) {
  const monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];
  return monthNames[monthIdx];
}

initLocalClocks();
